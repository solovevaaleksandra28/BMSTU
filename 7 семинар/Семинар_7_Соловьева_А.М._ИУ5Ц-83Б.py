# -*- coding: utf-8 -*-
import random, collections, queue

DEFAULT_NUMBER_OF_TAXIS = 5
DEFAULT_END_TIME = 500
SEARCH_DURATION = 5
TRIP_DURATION = 20
DEPARTURE_INTERVAL = 5

Event = collections.namedtuple('событие', 'время ID состояние')
trip_count = []  # для счетчиков поездок
trip_duration = []  # для суммарного времени поездок с пассжиром
trip_livetime = []  # для суммарного времени поездок


# начало функции TAXI_PROCESS
def taxi_process(ident, trips, start_time=0):  # <1>
    global trip_count, trip_duration
    counter = 0
    minutes = 0
    # выдать текущее состояние объекта-такси планировщику модели
    time = yield Event(start_time, ident, 'выехал из гаража')  # <2>
    for i in range(trips):  # <3>
        counter += 1
        minutes -= time
        time = yield Event(time, ident, 'посадил пассажира')  # <4>
        #
        minutes += time
        time = yield Event(time, ident, 'высадил пассажира')  # <5>

    trip_livetime[ident] = -start_time + time
    trip_duration[ident] = minutes
    trip_count[ident] = counter
    yield Event(time, ident, 'уехал в гараж')  # <6>


# конец TAXI_PROCESS # <7>

# начало класса TAXI_SIMULATOR
class Simulator:
    global trip_count, trip_duration, trip_livetime

    def __init__(self, procs_map):
        self.events = queue.PriorityQueue()
        self.procs = dict(procs_map)
        self.msglist = []  # для журнала событий

    def run(self, end_time):  # <1>
        # планировать и показывать события до конца модельного периода
        # запланировать первое событие для такси
        for _, proc in sorted(self.procs.items()):  # <2>
            first_event = next(proc)  # <3>
            self.events.put(first_event)  # <4>

        # основной цикл модели
        sim_time = 0  # <5>
        while sim_time <= end_time:  # <6>
            if self.events.empty():  # <7>
                self.msg2log('***процессы закончились! время=' + str(sim_time))
                print('***процессы закончились! время=' + str(sim_time))
                break

            current_event = self.events.get()  # <8>
            sim_time, proc_id, previous_action = current_event  # <9>

            if (previous_action == 'уехал в гараж'):
                print('такси: ', proc_id, ' ', proc_id * ' ', current_event, sep='')  # <10>
                self.msg2log(
                    str(proc_id) + ': {:>3d}'.format(sim_time) + ' >>' + previous_action + '; Кол-во поездок:' + str(
                        trip_count[proc_id]) + '; Длительность поездок с пассажиром:' + str(
                        trip_duration[proc_id]) + '; Общая длительность: ' + str(trip_livetime[proc_id]))
            else:
                print('такси: ', proc_id, ' ', proc_id * ' ', current_event, sep='')  # <10>
                self.msg2log(str(proc_id) + ': {:>3d}'.format(sim_time) + ' >>' + previous_action)

            active_proc = self.procs[proc_id]  # <11>
            next_time = sim_time + compute_duration(previous_action)  # <12>
            try:
                next_event = active_proc.send(next_time)  # <13>
            except StopIteration:
                del self.procs[proc_id]  # <14>
            else:
                self.events.put(next_event)  # <15>
        else:  # <16>
            msg = '*** время ' + str(end_time) + ' закончилось: {} процесса осталось ***'
            self.msg2log(msg.format(self.events.qsize()))
            print(msg.format(self.events.qsize()))

    def msg2log(self, msgstring):
        # добавление сообщений в журнал
        self.msglist.append(msgstring)

    def logs(self):
        for msg in self.msglist:
            print(msg)


# конец класса TAXI_SIMULATOR

def compute_duration(previous_action):
    # определить вид состояния и длительность интервала
    if previous_action in ['выехал из гаража', 'высадил пассажира']:
        # новое состояние - "поиск"
        interval = SEARCH_DURATION
    elif previous_action == 'посадил пассажира':
        # новое состояние - "поездка"
        interval = TRIP_DURATION
    elif previous_action == 'уехал в гараж':
        # новое состояние - "в парк"
        interval = 4
    else:
        # raise ValueError('Неизвестное действие: %s' % previous_action)
        interval = 5
    return random.randint(interval - 4, interval + 5)


def main(end_time=DEFAULT_END_TIME, num_taxis=DEFAULT_NUMBER_OF_TAXIS, seed=None):
    global trip_count, trip_duration, trip_livetime
    # инициализация датчика случайных чисел
    if seed is not None:  random.seed(seed)
    # создание множества такси в виде словаря
    taxis = {i: taxi_process(i, (i + 1) * 3, i * DEPARTURE_INTERVAL)
             for i in range(num_taxis)}
    trip_count = [0, ] * num_taxis
    trip_duration = [0, ] * num_taxis
    trip_livetime = [0, ] * num_taxis
    sim = Simulator(taxis)
    sim.run(end_time)
    sim.logs()


main()
